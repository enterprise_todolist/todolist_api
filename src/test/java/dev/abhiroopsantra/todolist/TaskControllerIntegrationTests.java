package dev.abhiroopsantra.todolist;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import dev.abhiroopsantra.todolist.service.impl.TaskService;
import io.github.cdimascio.dotenv.Dotenv;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import io.restassured.RestAssured;
import java.security.Key;
import java.util.Base64;
import java.util.Date;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@Testcontainers
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TaskControllerIntegrationTests {

  private static final MongoDBContainer mongoDBContainer;

  static {
    mongoDBContainer = new MongoDBContainer("mongo:latest");
    mongoDBContainer.start();
  }

  @Autowired
  TaskService taskService;
  @LocalServerPort
  private Integer port;

  private String token;

  @DynamicPropertySource
  static void mongoDbProperties(DynamicPropertyRegistry registry) {
    registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
  }

  @BeforeEach
  void setUp() {
    RestAssured.port = port;
    registerTestUser();
    token = generateToken();
  }

  private void registerTestUser() {
    String newUser =
        "{\"firstName\": \"John\", \"lastName\": \"Doe\", \"email\": \"john.doe@example.com\", \"password\": \"password123\"}";

    given()
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(newUser)
        .when()
        .post("/api/auth/register");
  }

  private String generateToken() {
    Dotenv dotenv = Dotenv.configure().load();
    String secretKey = dotenv.get("JWT_SECRET_KEY");
    byte[] keyBytes = Base64.getDecoder().decode(secretKey);
    Key key = Keys.hmacShaKeyFor(keyBytes);

    return Jwts.builder()
        .setSubject("john.doe@example.com")
        .setIssuedAt(new Date())
        .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60))
        .signWith(key, SignatureAlgorithm.HS512)
        .compact();
  }

  @Test
  @Order(3)
  void testGetAllTasks() {
    given()
        .header("Authorization", "Bearer " + token)
        .when()
        .get("api/task/fetch-all")
        .then()
        .statusCode(HttpStatus.OK.value())
        .contentType(MediaType.APPLICATION_JSON_VALUE);
  }

  @Test
  @Order(1)
  void testCreateTask() {
    String newTask =
        "{\"taskName\": \"Buy groceries\", \"taskDescription\": \"Chicken, eggs, milk\", \"dueDate\": \"2025-07-23T15:30:00\", \"priority\": \"MEDIUM\", \"remindAt\": \"2025-07-23T12:00:00\"}";

    given()
        .header("Authorization", "Bearer " + token)
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(newTask)
        .when()
        .post("api/task/create")
        .then()
        .statusCode(HttpStatus.CREATED.value())
        .body("data.taskName", equalTo("Buy groceries"))
        .body("data.taskDescription", equalTo("Chicken, eggs, milk"));
  }

  @Test
  @Order(2)
  void testUpdateTask() {
    String taskId = createTestTask();
    String updatedTask = "{\"taskName\": \"Updated Task\", \"taskDescription\": \"Updated description\"}";

    given()
        .header("Authorization", "Bearer " + token)
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(updatedTask)
        .when()
        .put("api/task/update/" + taskId)
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("data.taskName", equalTo("Updated Task"))
        .body("data.taskDescription", equalTo("Updated description"));
  }

  @Test
  @Order(4)
  void testDeleteTask() {
    String taskId = createTestTask();

    given()
        .header("Authorization", "Bearer " + token)
        .when()
        .delete("api/task/delete/" + taskId)
        .then()
        .statusCode(HttpStatus.NO_CONTENT.value());
  }

  private String createTestTask() {
    String newTask = "{\"taskName\": \"Practice shooting\", \"taskDescription\": \"Go to the shooting range and fire rifles\", \"dueDate\": \"2025-07-23T15:30:00\", \"priority\": \"MEDIUM\", \"remindAt\": \"2025-07-23T12:00:00\"}";

    return given()
        .header("Authorization", "Bearer " + token)
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(newTask)
        .when()
        .post("api/task/create")
        .then()
        .statusCode(HttpStatus.CREATED.value())
        .extract()
        .path("data.taskId");
  }
}
