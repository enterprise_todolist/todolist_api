package dev.abhiroopsantra.todolist;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import dev.abhiroopsantra.todolist.constant.TaskPriority;
import dev.abhiroopsantra.todolist.dto.TaskRequestDto;
import dev.abhiroopsantra.todolist.service.impl.TaskService;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;

@SpringBootTest
public class TaskServiceIntegrationTests {

  private static final MongoDBContainer mongoDBContainer;

  static {
    mongoDBContainer = new MongoDBContainer("mongo:latest");
    mongoDBContainer.start();
  }

  @Autowired
  private TaskService taskService;

  @DynamicPropertySource
  static void mongoDbProperties(DynamicPropertyRegistry registry) {
    registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
  }

  @Test
  public void shouldFetchAllTasks() {
    // Setup test data
    TaskRequestDto task1 = new TaskRequestDto();
    task1.setTaskName("Task 1");
    task1.setTaskDescription("Description 1");
    task1.setRemindAt(LocalDateTime.now().plusDays(1));
    task1.setDueDate(LocalDateTime.now().plusDays(7));
    taskService.createTask(task1);

    TaskRequestDto task2 = new TaskRequestDto();
    task2.setTaskName("Task 2");
    task2.setTaskDescription("Description 2");
    task2.setRemindAt(LocalDateTime.now().plusDays(2));
    task2.setDueDate(LocalDateTime.now().plusDays(7));
    taskService.createTask(task2);

    // Invoke the service method
    var tasksList = taskService.fetchAllTasks(0, 10, "taskName", "asc");

    // Verify the results
    assertNotNull(tasksList);
    assert tasksList.tasks.size() <= 10;
  }

  @Test
  public void shouldCreateTask() {
    // Setup test data
    TaskRequestDto task = new TaskRequestDto();
    task.setTaskName("Task 1");
    task.setTaskDescription("Description 1");
    task.setRemindAt(LocalDateTime.now().plusDays(1));
    task.setDueDate(LocalDateTime.now().plusDays(7));
    task.setPriority(TaskPriority.HIGH);

    // Invoke the service method
    var savedTask = taskService.createTask(task);

    // Verify the results
    assertNotNull(savedTask);
    assertNotNull(savedTask.getTaskId());
    assertEquals("Task 1", savedTask.getTaskName());
    assertEquals("Description 1", savedTask.getTaskDescription());
    assertEquals(task.getRemindAt(), savedTask.getRemindAt());
    assertEquals(task.getDueDate(), savedTask.getDueDate());
  }

  @Test
  public void shouldValidateTaskWithPastRemindAtDate() {
    // Setup test data
    TaskRequestDto task = new TaskRequestDto();
    task.setTaskName("Task 1");
    task.setTaskDescription("Description 1");
    task.setRemindAt(LocalDateTime.now().minusDays(1));
    task.setDueDate(LocalDateTime.now().plusDays(7));

    // Invoke the service method
    try {
      taskService.createTask(task);
    } catch (Exception e) {
      // Verify the results
      assertEquals("RemindAt date should be in the future", e.getMessage());
    }
  }

  @Test
  public void shouldValidateTaskWithPastDueDate() {
    // Setup test data
    TaskRequestDto task = new TaskRequestDto();
    task.setTaskName("Task 1");
    task.setTaskDescription("Description 1");
    task.setRemindAt(LocalDateTime.now().plusDays(1));
    task.setDueDate(LocalDateTime.now().minusDays(7));

    // Invoke the service method
    try {
      taskService.createTask(task);
    } catch (Exception e) {
      // Verify the results
      assertEquals("DueDate date should be in the future", e.getMessage());
    }
  }
}
