package dev.abhiroopsantra.todolist;


import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import dev.abhiroopsantra.todolist.service.impl.TaskService;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@Testcontainers
public class AuthControllerIntegrationTests {

  private static final MongoDBContainer mongoDBContainer;

  static {
    mongoDBContainer = new MongoDBContainer("mongo:latest");
    mongoDBContainer.start();
  }

  @Autowired
  TaskService taskService;
  @LocalServerPort
  private Integer port;

  @DynamicPropertySource
  static void mongoDbProperties(DynamicPropertyRegistry registry) {
    registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
  }

  @BeforeEach
  void setUp() {
    RestAssured.port = port;
  }

  @Test
  void shouldRegisterNewUser() {
    given().contentType(ContentType.JSON)
        .body("""
                {
                  "firstName": "John",
                  "lastName": "Doe",
                  "email": "john.doe@example.com",
                  "password": "password123"
                }
            """
        )
        .when()
        .post("/api/auth/register")
        .then()
        .statusCode(201)
        .body("token", notNullValue());
  }

  @Test
  void shouldLoginUser() {
    // First, register the user
    given().contentType(ContentType.JSON)
        .body("""
                {
                  "firstName": "Jane",
                  "lastName": "Doe",
                  "email": "jane.doe@example.com",
                  "password": "password123"
                }
            """
        )
        .when()
        .post("/api/auth/register")
        .then()
        .statusCode(201);

    // Then, login the user
    given().contentType(ContentType.JSON)
        .body("""
                {
                  "email": "jane.doe@example.com",
                  "password": "password123"
                }
            """
        )
        .when()
        .post("/api/auth/login")
        .then()
        .statusCode(200)
        .body("token", notNullValue());
  }

  @Test
  void shouldFailLoginWithInvalidCredentials() {
    given().contentType(ContentType.JSON)
        .body("""
                {
                  "email": "invalid@example.com",
                  "password": "wrongpassword"
                }
            """
        )
        .when()
        .post("/api/auth/login")
        .then()
        .statusCode(400)
        .body("errorMessage", equalTo("Password is incorrect or user is not registered"));
  }

  @Test
  void shouldValidateToken() {
    // First, register the user
    String token = given().contentType(ContentType.JSON)
        .body("""
                {
                  "firstName": "Alice",
                  "lastName": "Smith",
                  "email": "alice.smith@example.com",
                  "password": "password123"
                }
            """
        )
        .when()
        .post("/api/auth/register")
        .then()
        .statusCode(201)
        .extract()
        .path("token");

    // Then, validate the token
    given().contentType(ContentType.JSON)
        .header("Authorization", "Bearer " + token)
        .when()
        .get("/api/auth/validate")
        .then()
        .statusCode(200);
  }
}
