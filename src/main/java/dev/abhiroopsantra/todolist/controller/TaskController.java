package dev.abhiroopsantra.todolist.controller;

import dev.abhiroopsantra.todolist.constant.TaskConstants;
import dev.abhiroopsantra.todolist.dto.ResponseDto;
import dev.abhiroopsantra.todolist.dto.TaskRequestDto;
import dev.abhiroopsantra.todolist.dto.TaskResponseDto;
import dev.abhiroopsantra.todolist.exception.BadRequestException;
import dev.abhiroopsantra.todolist.service.ITaskService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/task", produces = {MediaType.APPLICATION_JSON_VALUE})
@Validated
@SecurityRequirement(name = "Bearer Authentication")
@Tag(
    name = "Task REST API",
    description = "CRUD REST API in Todo List to create, update, fetch list and delete task"
)
public class TaskController {

  private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(TaskController.class);
  private final ITaskService taskService;

  public TaskController(ITaskService taskService) {
    this.taskService = taskService;
  }

  @Operation(summary = "Create a new task")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "Task created successfully", content = {
          @Content(mediaType = "application/json",
              schema = @Schema(implementation = ResponseDto.class))}),
      @ApiResponse(responseCode = "400", description = "Invalid input",
          content = @Content),
      @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
      @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content),
      @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
  })
  @PostMapping(value = "/create")
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity<ResponseDto> createTask(@Valid @RequestBody TaskRequestDto taskRequestDto) {
    LOGGER.info("Creating a new task: {}", taskRequestDto);
    TaskResponseDto createdTask = taskService.createTask(taskRequestDto);
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(new ResponseDto(TaskConstants.STATUS_201, TaskConstants.MESSAGE_201, createdTask));
  }


  @Operation(summary = "Fetch all tasks")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Tasks fetched successfully", content = {
          @Content(mediaType = "application/json",
              schema = @Schema(implementation = ResponseDto.class))}),
      @ApiResponse(responseCode = "400", description = "Invalid input",
          content = @Content),
      @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
      @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content),
      @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
  })
  @GetMapping(value = "/fetch-all")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<ResponseDto> fetchAllTasks(Integer pageNumber, Integer pageSize,
      String sortBy, String sortDirection) {
    LOGGER.info("Fetching all tasks");

    int defaultPageNumber = TaskConstants.DEFAULT_PAGE_NUMBER;
    int defaultPageSize = TaskConstants.DEFAULT_PAGE_SIZE;

    pageNumber = (pageNumber != null) ? pageNumber : defaultPageNumber;
    pageSize = (pageSize != null) ? pageSize : defaultPageSize;

    if (sortBy == null || sortBy.isEmpty()) {
      sortBy = TaskConstants.DEFAULT_SORT_BY;
    }

    if (sortDirection == null || sortDirection.isEmpty()) {
      sortDirection = TaskConstants.DEFAULT_SORT_DIRECTION;
    } else if (!sortDirection.equalsIgnoreCase("asc")
        && !sortDirection.equalsIgnoreCase("desc")) {
      throw new BadRequestException("Invalid sort direction, must be 'asc' or 'desc'.");
    }

    return ResponseEntity.status(HttpStatus.OK)
        .body(new ResponseDto(TaskConstants.STATUS_200, TaskConstants.MESSAGE_200,
            taskService.fetchAllTasks(pageNumber, pageSize, sortBy, sortDirection)));
  }

  @Operation(summary = "Fetch all tasks")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Tasks fetched successfully", content = {
          @Content(mediaType = "application/json",
              schema = @Schema(implementation = ResponseDto.class))}),
      @ApiResponse(responseCode = "400", description = "Invalid input",
          content = @Content),
      @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
      @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content),
      @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
  })
  @GetMapping(value = "/fetch/{taskId}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<ResponseDto> fetchTaskDetails(@PathVariable String taskId) {
    return ResponseEntity.status(HttpStatus.OK)
        .body(new ResponseDto(TaskConstants.STATUS_200, TaskConstants.MESSAGE_200,
            taskService.fetchTaskById(taskId)));
  }

  @Operation(summary = "Update a task")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Task updated successfully", content = {
          @Content(mediaType = "application/json",
              schema = @Schema(implementation = ResponseDto.class))}),
      @ApiResponse(responseCode = "400", description = "Invalid input",
          content = @Content),
      @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
      @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content),
      @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
  })
  @PutMapping(value = "/update/{taskId}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<ResponseDto> UpdateTask(@PathVariable String taskId,
      @Valid @RequestBody TaskRequestDto taskRequestDto) {
    LOGGER.info("Updating a task");
    TaskResponseDto updatedTask = taskService.updateTask(taskId, taskRequestDto);
    return ResponseEntity.status(HttpStatus.OK)
        .body(new ResponseDto(TaskConstants.STATUS_200, TaskConstants.MESSAGE_200, updatedTask));
  }


  @Operation(summary = "Delete a task")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "204", description = "Task deleted successfully", content = {
          @Content(mediaType = "application/json",
              schema = @Schema(implementation = ResponseDto.class))}),
      @ApiResponse(responseCode = "400", description = "Invalid input",
          content = @Content),
      @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
      @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content),
      @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
  })
  @DeleteMapping(value = "/delete/{taskId}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public ResponseEntity<ResponseDto> DeleteTask(@PathVariable String taskId) {
    LOGGER.info("Deleting a task");
    taskService.deleteTaskById(taskId);
    return ResponseEntity.status(HttpStatus.NO_CONTENT)
        .body(new ResponseDto(TaskConstants.STATUS_204, TaskConstants.MESSAGE_200, null));
  }
}
