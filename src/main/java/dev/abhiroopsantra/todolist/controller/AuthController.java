package dev.abhiroopsantra.todolist.controller;

import dev.abhiroopsantra.todolist.dto.LoginRequestDto;
import dev.abhiroopsantra.todolist.dto.LoginResponseDto;
import dev.abhiroopsantra.todolist.dto.RegisterRequestDto;
import dev.abhiroopsantra.todolist.service.IAuthService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/auth", produces = {MediaType.APPLICATION_JSON_VALUE})
@RequiredArgsConstructor
@Tag(
    name = "Authentication REST API",
    description = "REST API for user registration and login"
)
public class AuthController {

  private final IAuthService authService;

  @Operation(summary = "Register a new user")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "User registered successfully", content = {
          @Content(mediaType = "application/json",
              schema = @Schema(implementation = LoginResponseDto.class))}),
      @ApiResponse(responseCode = "400", description = "Invalid input",
          content = @Content),
      @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
  })
  @PostMapping(value = "/register")
  public ResponseEntity<LoginResponseDto> register(@RequestBody RegisterRequestDto request) {
    return ResponseEntity.status(HttpStatus.CREATED).body(authService.register(request));
  }

  @Operation(summary = "Login user")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "User logged in successfully", content = {
          @Content(mediaType = "application/json",
              schema = @Schema(implementation = LoginResponseDto.class))}),
      @ApiResponse(responseCode = "400", description = "Invalid input",
          content = @Content),
      @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
  })
  @PostMapping(value = "/login")
  public ResponseEntity<LoginResponseDto> register(@RequestBody LoginRequestDto request) {
    return ResponseEntity.status(HttpStatus.OK).body(authService.login(request));
  }

  @Operation(summary = "Validate JWT token")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Token is valid", content = @Content),
      @ApiResponse(responseCode = "401", description = "Invalid token", content = @Content)
  })
  @GetMapping(value = "/validate")
  public ResponseEntity<Void> validateToken(
      @RequestHeader("Authorization") String authorizationHeader) {
    String token = authorizationHeader.replace("Bearer ", "");
    boolean isValid = authService.validateToken(token);
    return isValid ? ResponseEntity.ok().build()
        : ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
  }

}
