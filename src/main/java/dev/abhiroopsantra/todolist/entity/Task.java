package dev.abhiroopsantra.todolist.entity;

import dev.abhiroopsantra.todolist.constant.TaskPriority;
import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Document(collection = "tasks")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Task extends BaseEntity {

  @MongoId(FieldType.OBJECT_ID)
  private String taskId;

  private String parentTaskId;

  private String taskName;

  private String taskDescription;

  private LocalDateTime dueDate;

  private boolean isCompleted;
  
  private boolean isActive;

  private TaskPriority priority;

  private LocalDateTime remindAt;

  private String assignedTo; // Store User's _id as a reference

  private List<Task> subTasks;
}
