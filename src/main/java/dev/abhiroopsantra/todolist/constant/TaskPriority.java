package dev.abhiroopsantra.todolist.constant;

public enum TaskPriority {
  CRITICAL,
  HIGH,
  MEDIUM,
  LOW
}
