package dev.abhiroopsantra.todolist.constant;

public enum Role {
  USER,
  ADMIN
}
