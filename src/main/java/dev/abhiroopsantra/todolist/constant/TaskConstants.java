package dev.abhiroopsantra.todolist.constant;

public class TaskConstants {

  public static final String STATUS_201 = "201";
  public static final String STATUS_204 = "204";
  public static final String STATUS_200 = "200";
  public static final String STATUS_500 = "500";
  public static final String MESSAGE_500 = "An error occurred. Please try again later or contact the administrator.";
  public static final String MESSAGE_200 = "The request processed successfully.";
  public static final String MESSAGE_201 = "Successfully created.";

  public static final int DEFAULT_PAGE_NUMBER = 1;
  public static final int DEFAULT_PAGE_SIZE = 10;
  public static final String DEFAULT_SORT_BY = "createdAt";
  public static final String DEFAULT_SORT_DIRECTION = "desc";
}
