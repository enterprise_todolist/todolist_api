package dev.abhiroopsantra.todolist.exception;

import dev.abhiroopsantra.todolist.dto.ErrorResponseDto;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status,
      WebRequest request) {

    Map<String, String> validationErrors = new HashMap<>();
    List<ObjectError> validationErrorsList = ex.getBindingResult().getAllErrors();

    validationErrorsList.forEach(error -> {
      String fieldName = error.getObjectName();
      String validationMessage = error.getDefaultMessage();
      validationErrors.put(fieldName, validationMessage);
    });

    return new ResponseEntity<>(validationErrors, headers, status);
  }

  // Exception handler for ResourceNotFoundException
  @ExceptionHandler
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ResponseEntity<ErrorResponseDto> handleResourceNotFoundException(
      ResourceNotFoundException ex, WebRequest request) {
    ErrorResponseDto errorResponse = new ErrorResponseDto(request.getDescription(false),
        HttpStatus.NOT_FOUND, ex.getMessage(), LocalDateTime.now());
    return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
  }

  // Exception handler for BadRequestException
  @ExceptionHandler
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ResponseEntity<ErrorResponseDto> handleBadRequestException(BadRequestException ex,
      WebRequest request) {
    ErrorResponseDto errorResponse = new ErrorResponseDto(request.getDescription(false),
        HttpStatus.BAD_REQUEST, ex.getMessage(), LocalDateTime.now());
    return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
  }

  // Exception handler for all other exceptions
  @ExceptionHandler
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ResponseEntity<ErrorResponseDto> handleGlobalException(Exception ex, WebRequest request) {
    ErrorResponseDto errorResponse = new ErrorResponseDto(request.getDescription(false),
        HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), LocalDateTime.now());
    return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
