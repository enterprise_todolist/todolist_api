package dev.abhiroopsantra.todolist.repository;

import dev.abhiroopsantra.todolist.entity.Task;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends MongoRepository<Task, String> {

  List<Task> findByAssignedTo(String assignedTo);

  List<Task> findByParentTaskId(String parentTaskId);

}
