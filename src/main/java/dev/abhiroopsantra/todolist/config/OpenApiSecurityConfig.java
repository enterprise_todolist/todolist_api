package dev.abhiroopsantra.todolist.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition(
    info = @Info(
        title = "Enterprise Todo List API",
        version = "${api.version}",
        contact = @Contact(
            name = "Abhiroop Santra", email = "abhiroop.santra@example.com", url = "https://abhiroopsantra.dev"
        ),
        license = @License(
            name = "Apache 2.0", url = "https://www.apache.org/licenses/LICENSE-2.0"
        ),
        termsOfService = "${tos.uri}",
        description = "${api.description}"
    ),
    servers = @Server(
        url = "${api.server.url}",
        description = "${api.environment}"
    )
)
//@io.swagger.v3.oas.annotations.security.SecurityScheme(
//    name = "Bearer Authentication",
//    type = SecuritySchemeType.HTTP,
//    bearerFormat = "JWT",
//    scheme = "bearer"
//)
public class OpenApiSecurityConfig {

  @Bean
  public OpenAPI customizeOpenAPI() {
    final String securitySchemeName = "Bearer Authentication";
    return new OpenAPI()
        .addSecurityItem(new SecurityRequirement()
            .addList(securitySchemeName))
        .components(new Components()
            .addSecuritySchemes(securitySchemeName, new SecurityScheme()
                .name(securitySchemeName)
                .type(SecurityScheme.Type.HTTP)
                .scheme("bearer")
                .bearerFormat("JWT")));
  }
}
