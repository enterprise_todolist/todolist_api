package dev.abhiroopsantra.todolist.audit;

import dev.abhiroopsantra.todolist.utilities.JwtUtil;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

@Component("auditAwareImpl")
public class AuditAwareImpl implements AuditorAware<String> {

  private final HttpServletRequest request;

  private final JwtUtil jwtUtil;

  public AuditAwareImpl(HttpServletRequest request, JwtUtil jwtUtil) {
    this.request = request;
    this.jwtUtil = jwtUtil;
  }

  @Override
  public @NotNull Optional<String> getCurrentAuditor() {
    // return Optional.of("System");

    String currentUser = jwtUtil.extractUsername(request);

    if (currentUser == null) {
      return Optional.of("System");
    }

    return Optional.of(currentUser);
  }
}