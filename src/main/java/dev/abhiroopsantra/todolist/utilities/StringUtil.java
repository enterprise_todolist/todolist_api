package dev.abhiroopsantra.todolist.utilities;

public class StringUtil {

  public static String safeToString(Object obj) {
    return obj != null ? obj.toString() : "";
  }
}
