package dev.abhiroopsantra.todolist.utilities;

import org.bson.types.ObjectId;

public class CustomValidators {

  public static boolean isValidObjectId(String id) {
    return ObjectId.isValid(id);
  }
}
