package dev.abhiroopsantra.todolist.utilities;

import dev.abhiroopsantra.todolist.service.IJwtService;
import io.github.cdimascio.dotenv.Dotenv;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import jakarta.servlet.http.HttpServletRequest;
import java.security.Key;
import org.springframework.stereotype.Component;

@Component
public class JwtUtil {

  private final Key signingKey;

  public JwtUtil(Dotenv dotenv, IJwtService jwtService) {
    String secretKey = dotenv.get("JWT_SECRET_KEY");
    this.signingKey = jwtService.generateSigningKey(secretKey);
  }

  public String extractUsername(HttpServletRequest request) {
    String authorizationHeader = request.getHeader("Authorization");
    if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
      String token = authorizationHeader.substring(7);
      Claims claims = Jwts.parserBuilder()
          .setSigningKey(signingKey)
          .build()
          .parseClaimsJws(token)
          .getBody();
      return claims.getSubject();
    }
    return null;
  }


}
