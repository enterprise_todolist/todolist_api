package dev.abhiroopsantra.todolist.service.impl;

import dev.abhiroopsantra.todolist.constant.Role;
import dev.abhiroopsantra.todolist.dto.LoginRequestDto;
import dev.abhiroopsantra.todolist.dto.LoginResponseDto;
import dev.abhiroopsantra.todolist.dto.RegisterRequestDto;
import dev.abhiroopsantra.todolist.entity.User;
import dev.abhiroopsantra.todolist.exception.BadRequestException;
import dev.abhiroopsantra.todolist.repository.UserRepository;
import dev.abhiroopsantra.todolist.service.IAuthService;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService implements IAuthService {

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;
  private final JwtService jwtService;
  private final AuthenticationManager authenticationManager;

  /**
   * Register a new user
   *
   * @param request RegisterRequestDto
   * @return LoginResponseDto
   */
  @Override
  public LoginResponseDto register(RegisterRequestDto request) {

    // note: this function only registers users,
    // Admin role is to be assigned from DB for security reasons

    if (userRepository.findByEmail(request.getEmail()).isPresent()) {
      throw new BadRequestException("User with this email already exists");
    }

    User user = User.builder()
        .firstName(request.getFirstName())
        .lastName(request.getLastName())
        .email(request.getEmail())
        .password(passwordEncoder.encode(request.getPassword()))
        .role(Role.USER)
        .isEnabled(true)
        .build();

    userRepository.save(user);
    String jwtToken = jwtService.generateToken(user);

    return LoginResponseDto.builder()
        .token(jwtToken)
        .build();
  }

  /**
   * Login an existing user
   *
   * @param request LoginRequestDto
   * @return LoginResponseDto
   */
  @Override
  public LoginResponseDto login(LoginRequestDto request) {
    try {
      authenticationManager.authenticate(
          new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
    } catch (BadCredentialsException e) {
      throw new BadRequestException("Password is incorrect or user is not registered");
    }

    Optional<User> user = userRepository.findByEmail(request.getEmail());

    if (user.isEmpty()) {
      throw new BadRequestException("Password is incorrect or user is not registered");
    }

    String jwtToken = jwtService.generateToken(user.get());

    return LoginResponseDto.builder()
        .token(jwtToken)
        .build();
  }

  @Override
  public boolean validateToken(String token) {
    return jwtService.validateToken(token);
  }
}
