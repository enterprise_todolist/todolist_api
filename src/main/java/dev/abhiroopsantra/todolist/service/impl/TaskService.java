package dev.abhiroopsantra.todolist.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import dev.abhiroopsantra.todolist.constant.TaskConstants;
import dev.abhiroopsantra.todolist.dto.TaskListDto;
import dev.abhiroopsantra.todolist.dto.TaskMsgDto;
import dev.abhiroopsantra.todolist.dto.TaskRequestDto;
import dev.abhiroopsantra.todolist.dto.TaskResponseDto;
import dev.abhiroopsantra.todolist.entity.Task;
import dev.abhiroopsantra.todolist.exception.BadRequestException;
import dev.abhiroopsantra.todolist.exception.ResourceNotFoundException;
import dev.abhiroopsantra.todolist.mapper.TaskMapper;
import dev.abhiroopsantra.todolist.repository.TaskRepository;
import dev.abhiroopsantra.todolist.repository.UserRepository;
import dev.abhiroopsantra.todolist.service.ITaskService;
import dev.abhiroopsantra.todolist.utilities.StringUtil;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.bson.types.ObjectId;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TaskService implements ITaskService {

  private static final Logger LOGGER = LoggerFactory.getLogger(TaskService.class);
  private static final String TASK_CACHE = "taskCache";
  private static final String CACHE_PAGE_NUMBER = "pageNumber";
  private static final String CACHE_PAGE_SIZE = "pageSize";
  private static final String CACHE_SORT_BY = "sortBy";
  private static final String CACHE_SORT_DIRECTION = "sortDirection";

  private final StreamBridge streamBridge;
  private TaskRepository taskRepository;
  private UserRepository userRepository;

  private RedisTemplate<String, Object> redisTemplate;

  /**
   * Creates a new task.
   *
   * @param taskRequestDto the task DTO
   * @return TaskDto the created task DTO
   */
  @Override
//  @CachePut(value = TASK_CACHE, key = "'taskCache'")
  public TaskResponseDto createTask(TaskRequestDto taskRequestDto) {

    validateRequest(taskRequestDto);

    Task task = TaskMapper.mapToTask(taskRequestDto, new Task());
    Task savedTask = taskRepository.save(task);

//    updateCache();

    TaskResponseDto savedTaskRequestDto = TaskMapper.mapToTaskDto(savedTask, new TaskResponseDto());

    setAuditFields(savedTaskRequestDto, savedTask);
    sendTask(savedTask);

    return savedTaskRequestDto;
  }


  /**
   * Fetches all tasks.
   *
   * @param pageNumber    the page number
   * @param pageSize      the page size
   * @param sortBy        the field to sort by
   * @param sortDirection the direction to sort
   * @return List<TaskDto> the list of task DTOs
   */
  @Override
//  @Cacheable(value = TASK_CACHE, key = "'taskCache'")
  public TaskListDto fetchAllTasks(int pageNumber, int pageSize, String sortBy,
      String sortDirection) {

    if (Boolean.TRUE.equals(redisTemplate.hasKey(TASK_CACHE))) {
      try {
        String cachedData = (String) redisTemplate.opsForValue().get(TASK_CACHE);
        int pageNumberCache = Integer.parseInt(
            Objects.requireNonNull(redisTemplate.opsForValue().get(CACHE_PAGE_NUMBER)).toString());
        int pageSizeCache = Integer.parseInt(
            Objects.requireNonNull(redisTemplate.opsForValue().get(CACHE_PAGE_SIZE)).toString());
        String sortByCache = Objects.requireNonNull(redisTemplate.opsForValue().get(CACHE_SORT_BY))
            .toString();
        String sortDirectionCache = Objects.requireNonNull(
            redisTemplate.opsForValue().get(CACHE_SORT_DIRECTION)).toString();

        if (pageNumberCache != pageNumber || pageSizeCache != pageSize || !sortByCache.equals(
            sortBy)
            || !sortDirectionCache.equals(sortDirection)) {
          LOGGER.info("Cache miss. Fetching from database.");
          return fetchTasksFromDatabase(pageNumber, pageSize, sortBy, sortDirection);
        }

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        List<TaskResponseDto> cachedTasks = objectMapper.readValue(cachedData,
            new TypeReference<>() {
            });

        TaskListDto response = new TaskListDto();
        response.tasks = cachedTasks;
        response.totalCount = cachedTasks.size();
        response.pageSize = pageSize;
        response.currentPage = pageNumber;
        response.totalPages = (int) Math.ceil((double) cachedTasks.size() / pageSize);
        return response;
      } catch (Exception e) {
        LOGGER.warn(
            "Error while deserializing the cached data: {}. Continuing with database fetch.",
            e.getMessage());
      }
    }

    return fetchTasksFromDatabase(pageNumber, pageSize, sortBy, sortDirection);
  }


  /**
   * Fetches all tasks.
   *
   * @return List<TaskDto> the list of task DTOs
   */
  @Override
//  @CachePut(value = TASK_CACHE, key = "'taskCache'")
  public TaskResponseDto fetchTaskById(String taskId) {
    Task task = getTask(taskId);

    List<Task> subTasks = taskRepository.findByParentTaskId(taskId);

    task.setSubTasks(subTasks);

    TaskResponseDto taskResponseDto = TaskMapper.mapToTaskDto(task, new TaskResponseDto());
    setAuditFields(taskResponseDto, task);

    updateCache();

    // TODO: Set audit fields of sub tasks and check for nested sub tasks

    return taskResponseDto;
  }

  /**
   * Updates a task.
   *
   * @param taskRequestDto the task DTO
   * @return TaskDto the updated task DTO
   */
  @Override
//  @CachePut(value = TASK_CACHE, key = "'taskCache'")
  public TaskResponseDto updateTask(String taskId, TaskRequestDto taskRequestDto) {
    Task task = getTask(taskId);

    validateRequest(taskRequestDto);

    Task updatedTask = TaskMapper.mapToTask(taskRequestDto, task);

    // Save the updated task to the database
    Task savedTask = taskRepository.save(updatedTask);

//    updateCache();

    TaskResponseDto savedTaskRequestDto = TaskMapper.mapToTaskDto(savedTask, new TaskResponseDto());

    setAuditFields(savedTaskRequestDto, savedTask);
    sendTask(savedTask);

    return savedTaskRequestDto;
  }

  /**
   * Deletes a task.
   *
   * @param taskId the task ID
   * @return boolean the deletion status
   */
  @Override
//  @CachePut(value = TASK_CACHE, key = "'taskCache'")
  public boolean deleteTaskById(String taskId) {
    Task task = getTask(taskId);
    task.setActive(false);
    taskRepository.save(task);

//    updateCache();
    sendTask(task);

    return true;
  }

  /**
   * Updates the cache.
   */
  @Override
//  @CacheEvict(value = TASK_CACHE, key = "'taskCache'")
  public void updateCache() {
    Page<Task> result = getTasksList(TaskConstants.DEFAULT_PAGE_NUMBER,
        TaskConstants.DEFAULT_PAGE_SIZE,
        TaskConstants.DEFAULT_SORT_BY, TaskConstants.DEFAULT_SORT_DIRECTION);

    List<TaskResponseDto> recentTasks = transformToDto(result);

    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.registerModule(new JavaTimeModule());
    try {
      String jsonTasks = objectMapper.writeValueAsString(recentTasks);
      redisTemplate.opsForValue().set(TASK_CACHE, jsonTasks);
      redisTemplate.opsForValue().set(CACHE_PAGE_NUMBER, TaskConstants.DEFAULT_PAGE_NUMBER);
      redisTemplate.opsForValue().set(CACHE_PAGE_SIZE, TaskConstants.DEFAULT_PAGE_SIZE);
      redisTemplate.opsForValue().set(CACHE_SORT_BY, TaskConstants.DEFAULT_SORT_BY);
      redisTemplate.opsForValue().set(CACHE_SORT_DIRECTION, TaskConstants.DEFAULT_SORT_DIRECTION);
    } catch (Exception e) {
      LOGGER.warn("Error while serializing tasks to JSON: {}", e.getMessage());
    }
  }

  /**
   * Updates the cache.
   *
   * @param tasks         the list of task DTOs
   * @param pageNumber    the page number
   * @param pageSize      the page size
   * @param sortBy        the sort by field
   * @param sortDirection the sort direction
   */
  public void updateCache(List<TaskResponseDto> tasks, int pageNumber, int pageSize, String sortBy,
      String sortDirection) {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.registerModule(new JavaTimeModule()); // Register JavaTimeModule
    try {
      String jsonTasks = objectMapper.writeValueAsString(tasks);
      redisTemplate.opsForValue().set(TASK_CACHE, jsonTasks);
      redisTemplate.opsForValue().set(CACHE_PAGE_NUMBER, pageNumber);
      redisTemplate.opsForValue().set(CACHE_PAGE_SIZE, pageSize);
      redisTemplate.opsForValue().set(CACHE_SORT_BY, sortBy);
      redisTemplate.opsForValue().set(CACHE_SORT_DIRECTION, sortDirection);
    } catch (Exception e) {
      LOGGER.warn("Error while saving data to cache: {}", e.getMessage());
    }
  }

  private TaskListDto fetchTasksFromDatabase(int pageNumber, int pageSize, String sortBy,
      String sortDirection) {
    Page<Task> result = getTasksList(pageNumber, pageSize, sortBy, sortDirection);

    List<TaskResponseDto> tasks = transformToDto(result);

    TaskListDto response = new TaskListDto();
    response.tasks = tasks;
    response.totalCount = result.getTotalElements();
    response.pageSize = result.getSize();
    response.currentPage = result.getNumber() + 1;
    response.totalPages = result.getTotalPages();

    updateCache(tasks, pageNumber, pageSize, sortBy, sortDirection);

    return response;
  }

  private @NotNull List<TaskResponseDto> transformToDto(Page<Task> result) {
    return result.stream()
        .map(task -> {
          TaskResponseDto taskRequestDto = TaskMapper.mapToTaskDto(task, new TaskResponseDto());
          setAuditFields(taskRequestDto, task);
          return taskRequestDto;
        })
        .toList();
  }

  private @NotNull Page<Task> getTasksList(int pageNumber, int pageSize, String sortBy,
      String sortDirection) {
    if (pageNumber <= 0) {
      pageNumber = 1;
    }
    Sort.Direction direction = Sort.Direction.fromString(sortDirection);
    Pageable pageable = PageRequest.of(pageNumber - 1,
        pageSize,
        Sort.by(direction, sortBy));

    return taskRepository.findAll(pageable);
  }

  private Task getTask(String taskId) {
    Optional<Task> task = taskRepository.findById(taskId);

    if (task.isEmpty()) {
      throw new ResourceNotFoundException("Task not found with this taskId", "taskId", taskId);
    }
    return task.get();
  }

  private void sendTask(Task task) {
    TaskMsgDto taskMsgDto = new TaskMsgDto(task.getTaskName(),
        task.getTaskDescription(),
        StringUtil.safeToString(task.getDueDate()),
        task.isCompleted(),
        StringUtil.safeToString(task.getPriority()),
        StringUtil.safeToString(task.getRemindAt()),
        task.getParentTaskId(),
        task.getAssignedTo(),
        task.getCreatedBy(),
        StringUtil.safeToString(task.getCreatedAt()),
        task.getUpdatedBy(),
        StringUtil.safeToString(task.getUpdatedAt()));
    LOGGER.info("Sending task: {}", taskMsgDto);
    boolean sendResult = streamBridge.send("sendTask-out-0", taskMsgDto);
    LOGGER.info("Task send result: {}", sendResult);
  }

  private void validateRequest(TaskRequestDto taskRequestDto) {
    if (taskRequestDto.getRemindAt() != null && taskRequestDto.getRemindAt()
        .isBefore(LocalDateTime.now())) {
      throw new BadRequestException("RemindAt date should be in the future");
    }

    if (taskRequestDto.getDueDate() != null && taskRequestDto.getDueDate()
        .isBefore(LocalDateTime.now())) {
      throw new BadRequestException("DueDate date should be in the future");
    }

    if (taskRequestDto.getAssignedTo() != null
        && !ObjectId.isValid(taskRequestDto.getAssignedTo())) {
      throw new ResourceNotFoundException("No user found with this id", "assignedTo",
          taskRequestDto.getAssignedTo());
    }

    if (taskRequestDto.getAssignedTo() != null
        && !userRepository.existsById(taskRequestDto.getAssignedTo())) {
      throw new ResourceNotFoundException("No user found with this id", "assignedTo",
          taskRequestDto.getAssignedTo());
    }

  }

  private void setAuditFields(TaskResponseDto savedTaskResponseDto, Task savedTask) {
    // set the created and updated fields
    savedTaskResponseDto.setCreatedAt(savedTask.getCreatedAt());
    savedTaskResponseDto.setCreatedBy(savedTask.getCreatedBy());
    savedTaskResponseDto.setUpdatedAt(savedTask.getUpdatedAt());
    savedTaskResponseDto.setUpdatedBy(savedTask.getUpdatedBy());
  }

}
