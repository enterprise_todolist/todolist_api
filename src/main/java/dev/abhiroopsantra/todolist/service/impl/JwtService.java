package dev.abhiroopsantra.todolist.service.impl;

import dev.abhiroopsantra.todolist.service.IJwtService;
import io.github.cdimascio.dotenv.Dotenv;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class JwtService implements IJwtService {

  private final String secretKey;

  @Autowired
  public JwtService(Dotenv dotenv) {
    this.secretKey = dotenv.get("JWT_SECRET_KEY");
  }

  /**
   * Extracts the username from the token
   *
   * @param token the Bearer token
   * @return username extracted from the token
   */
  @Override
  public String extractUsername(String token) {
    return extractClaim(token, Claims::getSubject);
  }

  /**
   * Extracts the expiration date from the token
   *
   * @param userDetails the UserDetails object
   * @return the Bearer token
   */
  @Override
  public String generateToken(UserDetails userDetails) {
    return generateToken(new HashMap<>(), userDetails);
  }

  /**
   * Generates a token with the given claims
   *
   * @param extraClaims additional claims to be added to the token
   * @param userDetails the UserDetails object
   * @return the Bearer token
   */
  @Override
  public String generateToken(Map<String, Object> extraClaims, UserDetails userDetails) {
    return Jwts.builder()
        .setClaims(extraClaims)
        .setSubject(userDetails.getUsername())
        .setIssuedAt(new Date(System.currentTimeMillis()))
        .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60))
        .signWith(getsignInKey(), SignatureAlgorithm.HS512)
        .compact();
  }

  /**
   * Extracts the claim from the token
   *
   * @param token          the Bearer token
   * @param claimsResolver the function to resolve the claims
   * @param <T>            the type of the claim
   * @return the claim extracted from the token
   */
  @Override
  public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
    final Claims claims = extractAllClaims(token);
    return claimsResolver.apply(claims);
  }

  /**
   * Validates the token
   *
   * @param token       the Bearer token
   * @param userDetails the UserDetails object
   * @return true if the token is valid, false otherwise
   */
  @Override
  public Boolean isTokenValid(String token, UserDetails userDetails) {
    final String username = extractUsername(token);

    return username.equals(userDetails.getUsername()) && !isTokenExpired(token);
  }

  /**
   * Validates the token
   *
   * @param token the Bearer token
   * @return true if the token is valid, false otherwise
   */
  @Override
  public boolean validateToken(String token) {
    return !isTokenExpired(token);
  }

  /**
   * Returns true if the token is expired
   *
   * @param token the Bearer token
   * @return true if the token is expired, false otherwise
   */
  private boolean isTokenExpired(String token) {
    return extractExpiration(token).before(new Date());
  }

  /**
   * Extracts the expiration date from the token
   *
   * @param token the Bearer token
   * @return the expiration date extracted from the token
   */
  private Date extractExpiration(String token) {
    return extractClaim(token, Claims::getExpiration);
  }

  /**
   * Extracts all the claims from the token
   *
   * @param token the Bearer token
   * @return the claims extracted from the token
   */
  private Claims extractAllClaims(String token) {
    return Jwts
        .parserBuilder()
        .setSigningKey(getsignInKey())
        .build()
        .parseClaimsJws(token)
        .getBody();
  }

  /**
   * Generates a signing key
   *
   * @param secretKey the secret key
   * @return the signing key
   */
  public Key generateSigningKey(String secretKey) {
    byte[] keyBytes = Decoders.BASE64.decode(secretKey);
    return Keys.hmacShaKeyFor(keyBytes);
  }

  /**
   * Returns the sign-in key using the secret key
   *
   * @return the sign-in key
   */
  private Key getsignInKey() {
    return generateSigningKey(secretKey);
  }
}
