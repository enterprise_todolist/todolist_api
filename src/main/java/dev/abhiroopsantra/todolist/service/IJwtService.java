package dev.abhiroopsantra.todolist.service;

import io.jsonwebtoken.Claims;
import java.security.Key;
import java.util.Map;
import java.util.function.Function;
import org.springframework.security.core.userdetails.UserDetails;

public interface IJwtService {

  /**
   * Extracts the username from the token
   *
   * @param token the Bearer token
   * @return username extracted from the token
   */
  String extractUsername(String token);

  /**
   * Extracts the expiration date from the token
   *
   * @param userDetails the UserDetails object
   * @return the Bearer token
   */
  String generateToken(UserDetails userDetails);

  /**
   * Generates a token with the given claims
   *
   * @param extraClaims additional claims to be added to the token
   * @param userDetails the UserDetails object
   * @return the Bearer token
   */
  String generateToken(Map<String, Object> extraClaims, UserDetails userDetails);

  /**
   * Extracts the claim from the token
   *
   * @param token          the Bearer token
   * @param claimsResolver the function to resolve the claims
   * @param <T>            the type of the claim
   * @return the claim extracted from the token
   */
  <T> T extractClaim(String token, Function<Claims, T> claimsResolver);

  /**
   * Validates the token
   *
   * @param token       the Bearer token
   * @param userDetails the UserDetails object
   * @return true if the token is valid, false otherwise
   */
  Boolean isTokenValid(String token, UserDetails userDetails);

  /**
   * Validates the token
   *
   * @param token the Bearer token
   * @return true if the token is valid, false otherwise
   */
  boolean validateToken(String token);

  /**
   * Generates a signing key
   *
   * @param secretKey the secret key
   * @return the signing key
   */
  Key generateSigningKey(String secretKey);
}
