package dev.abhiroopsantra.todolist.service;

import dev.abhiroopsantra.todolist.dto.LoginRequestDto;
import dev.abhiroopsantra.todolist.dto.LoginResponseDto;
import dev.abhiroopsantra.todolist.dto.RegisterRequestDto;

public interface IAuthService {

  /**
   * Register a new user
   *
   * @param request RegisterRequestDto
   * @return LoginResponseDto
   */
  LoginResponseDto register(RegisterRequestDto request);

  /**
   * Login an existing user
   *
   * @param request LoginRequestDto
   * @return LoginResponseDto
   */
  LoginResponseDto login(LoginRequestDto request);

  boolean validateToken(String token);
}
