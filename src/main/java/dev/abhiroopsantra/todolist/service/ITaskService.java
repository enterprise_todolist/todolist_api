package dev.abhiroopsantra.todolist.service;

import dev.abhiroopsantra.todolist.dto.TaskListDto;
import dev.abhiroopsantra.todolist.dto.TaskRequestDto;
import dev.abhiroopsantra.todolist.dto.TaskResponseDto;
import java.util.List;

public interface ITaskService {

  /**
   * Creates a new task.
   *
   * @param taskRequestDto the task DTO
   * @return TaskDto the created task DTO
   */
  TaskResponseDto createTask(TaskRequestDto taskRequestDto);

  /**
   * Fetches all tasks.
   *
   * @param pageNumber the page number
   * @param pageSize   the page size
   * @return List<TaskDto> the list of task DTOs
   */
  TaskListDto fetchAllTasks(int pageNumber, int pageSize, String sortBy, String sortDirection);

  /**
   * Fetches all tasks.
   *
   * @return List<TaskDto> the list of task DTOs
   */
  TaskResponseDto fetchTaskById(String taskId);

  /**
   * Updates a task.
   *
   * @param taskRequestDto the task DTO
   * @return TaskDto the updated task DTO
   */
  TaskResponseDto updateTask(String taskId, TaskRequestDto taskRequestDto);

  /**
   * Deletes a task.
   *
   * @param taskId the task ID
   * @return boolean the deletion status
   */
  boolean deleteTaskById(String taskId);

  /**
   * Updates the cache.
   */
  void updateCache();

  /**
   * Updates the cache.
   *
   * @param tasks         the list of task DTOs
   * @param pageNumber    the page number
   * @param pageSize      the page size
   * @param sortBy        the sort by field
   * @param sortDirection the sort direction
   */
  void updateCache(List<TaskResponseDto> tasks, int pageNumber, int pageSize, String sortBy,
      String sortDirection);
}
