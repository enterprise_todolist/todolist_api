package dev.abhiroopsantra.todolist.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "LoginRequest", description = "User Login Request")
public class LoginRequestDto {

  @Schema(
      description = "Email of the user",
      example = "john.wayne@example.com")
  private String email;

  @Schema(
      description = "Password of the user",
      example = "MySecretPassword123!")
  private String password;
}
