package dev.abhiroopsantra.todolist.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class TaskResponseDto extends TaskBaseDto implements Serializable {

  @Serial
  private static final long serialVersionUID = 1L;

  @Schema(
      description = "Task id",
      example = "60f1b4b3b3b3b3b3b3b3b3"
  )
  private String taskId;

  @Schema(
      description = "When the task was first created",
      example = "2021-07-17T12:00:00"
  )
  private LocalDateTime createdAt;

  @Schema(
      description = "User who created the task",
      example = "john.doe@example.com"
  )
  private String createdBy;

  @Schema(
      description = "When the task was last updated",
      example = "2021-07-17T12:00:00"
  )
  private LocalDateTime updatedAt;

  @Schema(
      description = "User who last updated the task",
      example = "jane.doe@example.com"
  )
  private String updatedBy;

  @Schema(
      description = "List of sub-tasks belonging to the task"
  )
  private List<TaskResponseDto> subTasks;

  @Schema(
      description = "Task assigned to user id",
      example = "60f1b4b3b3b3b3b3b3b3b3"
  )
  private String assignedTo;

  @Schema(
      description = "Parent task id",
      example = "60f1b4b3b3b3b3b3b3b3b3"
  )
  private String parentTaskId;

  @Schema(
      description = "Task status (true for active, false for inactive)",
      example = "true"
  )
  private boolean isActive;
}
