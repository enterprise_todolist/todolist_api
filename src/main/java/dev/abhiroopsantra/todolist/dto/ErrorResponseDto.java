package dev.abhiroopsantra.todolist.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
@Schema(name = "Error Response", description = "Error Response for API calls")
public class ErrorResponseDto {

  @Schema(
      description = "API path where the error occurred",
      example = "/api/v1/tasks"
  )
  private String apiPath;

  @Schema(
      description = "HTTP status code of the error",
      example = "404"
  )
  private HttpStatus httpStatus;

  @Schema(
      description = "Error message",
      example = "Task not found"
  )
  private String errorMessage;

  @Schema(
      description = "Time when the error occurred",
      example = "2021-12-31T23:59:59"
  )
  private LocalDateTime errorTime;
}
