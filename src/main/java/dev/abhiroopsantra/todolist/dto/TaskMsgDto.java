package dev.abhiroopsantra.todolist.dto;

public record TaskMsgDto(String TaskName, String TaskDescription, String DueDate,
                         boolean IsCompleted, String Priority, String RemindAt, String parentTaskId,
                         String assignedTo, String createdBy, String createdAt, String updatedBy,
                         String updatedAt) {

}
