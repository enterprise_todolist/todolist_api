package dev.abhiroopsantra.todolist.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import lombok.Data;

@Data
@Schema(name = "TaskList", description = "Schema for list of tasks")
public class TaskListDto implements Serializable {

  @Serial
  private static final long serialVersionUID = 1L;

  @Schema(description = "List of tasks")
  public List<TaskResponseDto> tasks;

  @Schema(description = "Total count of tasks", example = "10")
  public long totalCount;

  @Schema(description = "Page size", example = "5")
  public int pageSize;

  @Schema(description = "Current page number", example = "1")
  public int currentPage;

  @Schema(description = "Total number of pages", example = "2")
  public int totalPages;
}
