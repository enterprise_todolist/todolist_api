package dev.abhiroopsantra.todolist.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "RegisterRequest", description = "Schema for User Registration information")
public class RegisterRequestDto {

  @Schema(
      description = "First name of the user",
      example = "John"
  )
  private String firstName;

  @Schema(
      description = "Last name of the user",
      example = "Wayne"
  )
  private String lastName;

  @Schema(
      description = "Email of the user",
      example = "john.wayne@example.com"
  )
  private String email;

  @Schema(
      description = "Password of the user",
      example = "MySecretPassword123!"
  )
  private String password;

//  @Schema(
//      description = "Role of the user (ADMIN, USER)"
//  )
//  private Role role;
}
