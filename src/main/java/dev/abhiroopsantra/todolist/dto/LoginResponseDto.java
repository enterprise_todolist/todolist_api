package dev.abhiroopsantra.todolist.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "LoginResponse", description = "API Response for User Login Action")
public class LoginResponseDto {

  @Schema(
      description = "JWT token"
  )
  private String token;

  @Schema(
      description = "Refresh Token which can be used to generate new JWT token")
  private String refreshToken;
}
