package dev.abhiroopsantra.todolist.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import dev.abhiroopsantra.todolist.constant.TaskPriority;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;

@Data
public abstract class TaskBaseDto implements Serializable {

  @Serial
  private static final long serialVersionUID = 1L;

  @NotBlank(message = "Task name cannot be blank")
  @Size(min = 3, max = 50, message = "Task name must be between 3 and 50 characters")
  @Schema(
      description = "Task name",
      example = "Buy groceries"
  )
  private String taskName;

  @Size(max = 500, message = "Task description must be less than 200 characters")
  @Schema(
      description = "Task description",
      example = "Buy groceries from the nearby store"
  )
  private String taskDescription;

  @Schema(
      description = "Task due date",
      example = "2021-12-31T23:59:59"
  )
  @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
  private LocalDateTime dueDate;

  @Schema(
      description = "Task status (true for completed, false for pending)",
      example = "true"
  )
  private boolean isCompleted;

  @Schema(
      description = "Task priority (CRITICAL, HIGH, MEDIUM, LOW)",
      example = "HIGH"
  )
  private TaskPriority priority;

  @Schema(
      description = "Task reminder date",
      example = "2021-12-31T23:59:59"
  )
  @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
  private LocalDateTime remindAt;
}
