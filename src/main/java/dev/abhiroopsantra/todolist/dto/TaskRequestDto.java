package dev.abhiroopsantra.todolist.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@Schema(name = "Task", description = "Schema to hold Task information")
public class TaskRequestDto extends TaskBaseDto {

  @Schema(
      description = "Parent task id",
      example = "60f1b4b3b3b3b3b3b3b3b3b3"
  )
  private String parentTaskId;

  @Schema(
      description = "User id to whom task is assigned",
      example = "60f1b4b3b3b3b3b3b3b3b3b3"
  )
  private String assignedTo;
}
