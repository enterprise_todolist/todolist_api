package dev.abhiroopsantra.todolist.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@Schema(name = "Response", description = "Response for API calls")
public class ResponseDto {

  @Schema(
      description = "Status code of the response",
      example = "200"
  )
  private String statusCode;

  @Schema(
      description = "Status message of the response",
      example = "Success"
  )
  private String statusMessage;

  @Schema(
      description = "A JSON object expected from the API call")
  private Object data;
}
