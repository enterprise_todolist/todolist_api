package dev.abhiroopsantra.todolist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableMongoAuditing(auditorAwareRef = "auditAwareImpl")
public class TodolistApplication {

  public static void main(String[] args) {
    SpringApplication.run(TodolistApplication.class, args);
  }

}
