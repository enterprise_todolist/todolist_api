package dev.abhiroopsantra.todolist.mapper;

import dev.abhiroopsantra.todolist.dto.TaskRequestDto;
import dev.abhiroopsantra.todolist.dto.TaskResponseDto;
import dev.abhiroopsantra.todolist.entity.Task;
import java.util.ArrayList;
import java.util.List;

public class TaskMapper {

  /**
   * Maps Task to TaskDto.
   *
   * @param task            The task persistence entity
   * @param taskResponseDto the task DTO
   * @return TaskDto the mapped task DTO
   */
  public static TaskResponseDto mapToTaskDto(Task task, TaskResponseDto taskResponseDto) {
    taskResponseDto.setTaskId(task.getTaskId());
    taskResponseDto.setTaskName(task.getTaskName());
    taskResponseDto.setTaskDescription(task.getTaskDescription());
    taskResponseDto.setDueDate(task.getDueDate());
    taskResponseDto.setCompleted(task.isCompleted());
    taskResponseDto.setActive(task.isActive());
    taskResponseDto.setPriority(task.getPriority());
    taskResponseDto.setRemindAt(task.getRemindAt());
    taskResponseDto.setAssignedTo(task.getAssignedTo());
    taskResponseDto.setParentTaskId(task.getParentTaskId());

    if (task.getSubTasks() != null && !task.getSubTasks().isEmpty()) {
      List<TaskResponseDto> subTasks = new ArrayList<>();

      for (Task subTask : task.getSubTasks()) {
        TaskResponseDto subTaskDto = new TaskResponseDto();
        mapToTaskDto(subTask, subTaskDto);
        subTasks.add(subTaskDto);
      }

      taskResponseDto.setSubTasks(subTasks);
    }

    return taskResponseDto;
  }

  /**
   * Maps TaskDto to Task.
   *
   * @param taskRequestDto the task DTO
   * @param task           The task persistence entity
   * @return Task the mapped task persistence entity
   */
  public static Task mapToTask(TaskRequestDto taskRequestDto, Task task) {
    task.setTaskName(taskRequestDto.getTaskName());
    task.setTaskDescription(taskRequestDto.getTaskDescription());
    task.setDueDate(taskRequestDto.getDueDate());
    task.setCompleted(taskRequestDto.isCompleted());
    task.setPriority(taskRequestDto.getPriority());
    task.setRemindAt(taskRequestDto.getRemindAt());
    task.setAssignedTo(taskRequestDto.getAssignedTo());
    task.setParentTaskId(taskRequestDto.getParentTaskId());

    return task;
  }
}
