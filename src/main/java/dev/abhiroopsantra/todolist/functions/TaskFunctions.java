package dev.abhiroopsantra.todolist.functions;

import dev.abhiroopsantra.todolist.dto.TaskMsgDto;
import dev.abhiroopsantra.todolist.service.ITaskService;
import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TaskFunctions {

  private static final Logger LOGGER = LoggerFactory.getLogger(TaskFunctions.class);

  @Bean
  public Consumer<TaskMsgDto> receiveTask(ITaskService taskService) {
    return taskMsgDto -> {
      LOGGER.info("Received and processing task: {}", taskMsgDto);
      taskService.updateCache();
    };
  }
}
